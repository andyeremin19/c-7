﻿#include <iostream>
class Animal
{
public:
	virtual void Voice()
	{
		std::cout<<"Bugaga"<<'\n';
	}
};
class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout <<"Bark" << '\n';
	}
};
class Cat :public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow" << '\n';
	}
};
class Parrot :public Animal
{
public:
	void Voice() override
	{
		std::cout << "Doodledo" << '\n';
	}
};
int main()
{
	Animal* p[3] = { new Dog ,new Cat, new Parrot };
	for (int i = 0; i < 3; i++)
	{
		p[i]->Voice();
	}
}

